-module(rebar3_deb_prv).

-export([init/1, do/1, format_error/1]).

% Some things from rebar.hrl
-define(CONSOLE(Str, Args), io:format(Str++"~n", Args)).
-define(DEBUG(Str, Args), rebar_log:log(debug, Str, Args)).
-define(INFO(Str, Args), rebar_log:log(info, Str, Args)).
-define(WARN(Str, Args), rebar_log:log(warn, Str, Args)).
-define(ERROR(Str, Args), rebar_log:log(error, Str, Args)).


-define(PROVIDER, deb).
-define(DEPS, [compile]).

-define(PATH_PREFIX, "smprc/erlang").

%% ===================================================================
%% Public API
%% ===================================================================
-spec init(rebar_state:t()) -> {ok, rebar_state:t()}.
init(State) ->
    Provider = providers:create([
            {name, ?PROVIDER},            % The 'user friendly' name of the task
            {module, ?MODULE},            % The module implementation of the task
            {bare, true},                 % The task can be run by the user, always true
            {deps, ?DEPS},                % The list of dependencies
            {example, "rebar3 deb"},      % How to use the plugin
            {opts, deb_opts(State)},      % list of options understood by the plugin
            {short_desc, "Pack your app to .deb package"},
            {desc, "Pack your app to .deb package"}
    ]),
    {ok, rebar_state:add_provider(State, Provider)}.


-spec do(rebar_state:t()) -> {ok, rebar_state:t()} | {error, string()}.
do(State) ->
    ProjApps = rebar_state:project_apps(State),
    pack_apps(State, ProjApps),
    {ok, State}.

-spec format_error(any()) ->  iolist().
format_error(Reason) ->
    io_lib:format("~p", [Reason]).


%% ===================================================================
%% Internals
%% ===================================================================

pack_apps(State, Apps) ->
    [pack_app(State, AppInfo) || AppInfo <- Apps].


pack_app(State, AppInfo) ->
    App = rebar_app_info:name(AppInfo),
    ?INFO("Packing ~s", [App]),
    Opts  = rebar_state:get(State, deb_opts, []),
    Dir = rebar_app_info:out_dir(AppInfo),
    BaseDir = rebar_dir:base_dir(State),
    PackDir = filename:join(BaseDir, "deb"),
    LibDir  = filename:join([PackDir, "var/lib/" ++ ?PATH_PREFIX, App]),
    DebDir  = filename:join(PackDir, "DEBIAN"),
    ControlFile = filename:join(DebDir, "control"),
    filelib:ensure_dir(ControlFile),
    case proplists:get_value(aux_dir, Opts) of
        undefined -> ok;
        AD -> 
            AuxDir = filename:join(rebar_dir:get_cwd(), AD),
            copy_install_files(State, AppInfo, AuxDir, PackDir)
    end,
    write_control(State, AppInfo, ControlFile),
    filelib:ensure_dir(filename:join(LibDir, "dummy")),
    ?DEBUG("Inspecting ~s", [Dir]),
    copy_lib(State, AppInfo, LibDir),
    create_package(State, AppInfo, PackDir).


write_control(State, AppInfo, File) ->
    {CmdOpts, _} = rebar_state:command_parsed_args(State),
    Opts  = rebar_state:get(State, deb_opts, []),
    App   = rebar_app_info:name(AppInfo),
    Name  = ec_cnv:to_binary(proplists:get_value(package, Opts, App)),
    Sect  = ec_cnv:to_binary(proplists:get_value(section, Opts, <<"misc">>)),
    Vsn   = ec_cnv:to_binary(proplists:get_value(version, Opts, "0.1.0")),
    Arch  = ec_cnv:to_binary(proplists:get_value(architecture, Opts, "all")),
    Maint = ec_cnv:to_binary(proplists:get_value(maintainer, Opts, <<>>)),
    Descr = case ec_cnv:to_binary(proplists:get_value(description, Opts, <<>>)) of
        <<>> -> rebar_utils:abort("DEB creation stopped: no package description found", []);
        Other -> Other
    end,
    Deps  = ec_cnv:to_binary(proplists:get_value(deps, Opts, <<>>)),
    BuildNo = case ec_cnv:to_binary(proplists:get_value(buildno, CmdOpts, <<>>)) of
        <<>> -> <<>>;
        B    -> <<$-, B/binary>>
    end,
    Control = <<
        "Package: ", Name/binary, "\n",
        "Section: ", Sect/binary, "\n",
        "Version: ", Vsn/binary, BuildNo/binary, "\n",
        "Architecture: ", Arch/binary, "\n",
        "Maintainer: ", Maint/binary, "\n",
        "Description: ", Descr/binary, "\n",
        "Depends: ", Deps/binary, "\n"
    >>,
    file:write_file(File, Control).


copy_lib(_State, AppInfo, LibDir) ->
    EbinDir = filename:join([rebar_app_info:out_dir(AppInfo), "ebin"]),
    OutEbin = filename:join([LibDir, "ebin"]),
    filelib:ensure_dir(filename:join([OutEbin, "dummy.erl"])),
    rebar_file_utils:cp_r(filelib:wildcard(filename:join([EbinDir, "*"])), OutEbin).


copy_install_files(_State, _AppInfo, FilesDir, DebDir) ->
    rebar_file_utils:cp_r(filelib:wildcard(filename:join([FilesDir, "*"])), DebDir).
    %?DEBUG("Copy ~s", [FilesDir]).


create_package(_State, _AppInfo, PackDir) ->
    Cmd = io_lib:format("dpkg-deb --build ~s ~s", [PackDir, PackDir]),
    rebar_utils:sh(Cmd, [{use_stdout,false}, abort_on_error]).


deb_opts(_State) ->
    [
        {package,       $p, "package", string, "Name of the package"},
        {section,       $s, "section", string, "This field specifies an application area into which the package has been classified"},
        {version,       $v, "version", string, "Version of the package"},
        {buildno,       $b, "buildno", string, "Build number"},
        {architecture,  $a, "architecture", string, "Debian machine architecture"},
        {maintainer,    $m, "version", string, "The package maintainer's name and email address. The name must come first, then the email address inside angle brackets <> (in RFC822 format)"},
        {deps,          $r, "deps",    string, "Declares an absolute dependency. A package will not be configured unless all of the packages listed in its Depends field have been correctly configured"},
        {description,   $d, "description", string, "Short description of the package"},
        {aux_dir,       $a, "auxdir", string, "Path to install scripts and auxiliary install files"}
    ].
