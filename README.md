rebar3-deb-plugin
=====

A rebar plugin for packing your app to .deb package

Build
-----

    $ rebar3 compile

Use
---

Add the plugin to your rebar config:

    {plugins, [
        { rebar3_deb, ".*", {git, "https://bitbucket.org/payouts/rebar3-deb-plugin.git", {branch, "master"}}}
    ]}.

Then just call your plugin directly in an existing application:


    $ rebar3 deb
    ===> Fetching rebar3_deb
    ===> Compiling rebar3_deb
    <Plugin Output>
